# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-13 18:16+0000\n"
"PO-Revision-Date: 2023-06-15 20:52+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/debexpo/locale/"
"es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18.1-dev\n"

#: settings/common.py
msgid "English"
msgstr "Inglés"

#: settings/common.py
msgid "German"
msgstr ""

#: settings/common.py
msgid "Spanish"
msgstr ""

#: settings/common.py
msgid "Persian"
msgstr ""

#: settings/common.py
msgid "French"
msgstr "Francés"

#: settings/common.py
msgid "Italian"
msgstr ""

#: settings/common.py
msgid "Japanese"
msgstr ""

#: settings/common.py
msgid "Dutch"
msgstr ""

#: settings/common.py
#, fuzzy
#| msgid "Portuguese (Brazil)"
msgid "Portuguese"
msgstr "Portugués (Brasil)"

#: settings/common.py
msgid "Portuguese (Brazil)"
msgstr "Portugués (Brasil)"

#: settings/common.py
msgid "Romanian"
msgstr ""

#: settings/common.py
msgid "Swedish"
msgstr ""

#: settings/common.py
msgid "Chinese (Simplified)"
msgstr ""

#: settings/common.py
msgid "Helps you get your packages into Debian"
msgstr "Te ayuda a introducir tus paquetes en Debian"

#: urls.py
msgid "Email change complete"
msgstr "Cambio del correo electrónico completado"
