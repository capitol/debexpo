# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2023-02-26 21:38+0000\n"
"Last-Translator: Frans Spiesschaert <frans.spiesschaert@gmail.com>\n"
"Language-Team: Dutch <https://hosted.weblate.org/projects/debexpo/nntp/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"

#: nntp/models.py
msgid "Namespace"
msgstr "Naamruimte"

#: nntp/models.py
msgid "List name"
msgstr "Lijstnaam"

#: nntp/models.py
msgid "Last message processed"
msgstr "Laatste verwerkte bericht"
