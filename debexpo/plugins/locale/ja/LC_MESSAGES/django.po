# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2022-11-23 19:30+0000\n"
"Last-Translator: Kentaro Hayashi <kenhys@gmail.com>\n"
"Language-Team: Japanese <https://hosted.weblate.org/projects/debexpo/plugins/"
"ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.15-dev\n"

#: plugins/models.py
msgid "Information"
msgstr "情報"

#: plugins/models.py
msgid "Warning"
msgstr "警告"

#: plugins/models.py
msgid "Error"
msgstr "エラー"

#: plugins/models.py
msgid "Critical"
msgstr "致命的"

#: plugins/models.py
msgid "Failed"
msgstr "失敗"

#: plugins/models.py
msgid "Plugin name"
msgstr "プラグイン名"

#: plugins/models.py
msgid "Test identifier"
msgstr "テスト識別子"

#: plugins/models.py
msgid "Outcome"
msgstr "結果"

#: plugins/models.py
msgid "Data"
msgstr "データ"

#: plugins/models.py
msgid "Severity"
msgstr "重要度"
